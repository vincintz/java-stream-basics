package com.samples.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Person
{
    private final long id;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Gender gender;
    private final LocalDate birthdate;
    private final String address;
    private final String phone;

    public Person(String line)
    {
        final String[] s = (line + "\t\t\t\t.").split("\t");
        id = Long.parseLong(s[0]);
        firstName = s[1];
        lastName = s[2];
        email = "".equals(s[3]) ? null : s[3];
        gender = Gender.valueOf(s[4]);
        birthdate = LocalDate.parse(s[5], DateTimeFormatter.ISO_DATE);
        address = "".equals(s[6]) ? null : s[8];
        phone = "".equals(s[7]) ? null : s[7];
    }

    public long getId()
    {
        return id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public Gender getGender()
    {
        return gender;
    }

    public LocalDate getBirthdate()
    {
        return birthdate;
    }

    public static List<Person> load()
    {
        try (final Stream<String> stream = Files.lines(Paths.get("src/main/resources/persons.txt"))) {
            return stream
                    .filter(s -> !"id\tfirst_name\tlast_name\temail\tgender\tbirthdate\taddress\tphone".equals(s))
                    .map(Person::new)
                    .collect(Collectors.toList());
        }
        catch (final IOException ex) {
            return Collections.EMPTY_LIST;
        }
    }

    public String getAddress()
    {
        return address;
    }

    public String getPhone()
    {
        return phone;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getId());
        builder.append("\t" + getLastName() + ", " + getFirstName());
        return builder.toString();
    }

    public enum Gender {
        Male, Female
    }
}
