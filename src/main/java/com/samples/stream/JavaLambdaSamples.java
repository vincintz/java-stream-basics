package com.samples.stream;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class JavaLambdaSamples {
    public static void main(final String[] args) {
        new JavaLambdaSamples()
            .sample1()
            .sample2()
            .sample3();
    }

    public JavaLambdaSamples sample1() {
        System.out.println("\n\nTODO: Replace comparator with lambda");
        // Replace comparator with lambda
        final List<Person> people = Person.load();
        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2)
            {
                return p1.getBirthdate().compareTo(p2.getBirthdate());
            }
        });
        return this;
    }

    public JavaLambdaSamples sample2() {
        System.out.println("\n\nTODO: Replace Runnable with lambda");
        // Replace Runnable with lambda
        final List<Person> people = Person.load();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (final Person p : people) {
                    System.out.println(p.getBirthdate() + " - " + p);
                }
            }
        });
        t.run();
        try {
            t.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    public JavaLambdaSamples sample3() {
        System.out.println("\n\nTODO: Replace with forEach");
        final List<Person> people = Person.load();
        for (final Person p : people) {
            System.out.println(p.getBirthdate() + " - " + p);
        }
        return this;
    }

    public JavaLambdaSamples syntax() {
        // Lambda syntax
        Runnable runnable =
                () -> {
                    System.out.println();
                };
        Comparator<Person> comparator =
                (p1, p2) -> p1.getBirthdate().compareTo(p2.getBirthdate());
        Predicate<Person> pred =
                p -> p.getPhone() != null || p.getEmail() != null;
        Function<Person, String> fun1 =
                (Person p) -> p.getFirstName() + " " + p.getLastName();
        BiFunction<Person, Person, Boolean> fun2 =
                (Person p1, Person p2) -> {
                    boolean b = p1.getFirstName() == p2.getFirstName();
                    return b;
                };
        Consumer<Person> consumer =
                p -> System.out.println(p.toString());
        return this;
    }

//    @FunctionalInterface
    private interface Foo {
        boolean method2();
        default boolean method1() {
            return true;
        }
//        boolean equals(Object o);
    }

}
