package com.samples.stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class JavaStreamSamples {
    public static void main(final String[] args) {
        new JavaStreamSamples()
            .sample1()
            .sample2()
            .sample3()
            .sample4();
    }

    private JavaStreamSamples sample1() {
        System.out.println("\n\nLook for the oldest person with either an email or phone");
        final List<Person> people = Person.load();
        Person oldestWithContact = null;
        for (final Person p : people) {
            if (p.getEmail() != null || p.getPhone() != null) {
                if (oldestWithContact == null ||
                        p.getBirthdate().isBefore(oldestWithContact.getBirthdate())) {
                    oldestWithContact = p;
                }
            }
        }
        System.out.println(oldestWithContact.getBirthdate() + " - " + oldestWithContact);
        return this;
    }

    private JavaStreamSamples sample2() {
        System.out.println("\n\nLook for the oldest 3 people with either an email or phone");
        final List<Person> people = Person.load();
        people.sort(new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                return p1.getBirthdate().compareTo(p2.getBirthdate());
            }
        });
        List<Person> topThreeOldestWithContact = new ArrayList<Person>();
        for (final Person p : people) {
            if (p.getEmail() != null || p.getPhone() != null) {
                topThreeOldestWithContact.add(p);
                if (topThreeOldestWithContact.size() >= 3) {
                    break;
                }
            }
        }
        for (final Person p : topThreeOldestWithContact) {
            System.out.println(p.getBirthdate() + " - " + p);
        }
        return this;
    }

    private JavaStreamSamples sample3() {
        System.out.println("\n\nCount the number of people with either an email or phone");
        final List<Person> people = Person.load();
        int count = 0;
        for (final Person p : people) {
            if (p.getEmail() != null || p.getPhone() != null) {
                count++;
            }
        }
        System.out.println("Count: " + count);
        return this;
    }

    private JavaStreamSamples sample4() {
        System.out.println("\n\nCount the number of vowels in peoples names");
        final List<Person> people = Person.load();
        int count = 0;
        for (final Person p : people) {
            if (p.getEmail() != null || p.getPhone() != null) {
                count += vowels(p.getFirstName()) + vowels(p.getLastName());
            }
        }
        System.out.println("Count: " + count);
        return this;
    }

    private int vowels(final String s) {
        int vowels = 0;
        for (int i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case 'A':
                case 'a':
                case 'E':
                case 'e':
                case 'I':
                case 'i':
                case 'O':
                case 'o':
                case 'U':
                case 'u':
                    vowels++;
                    break;
            }
        }
        return vowels;
    }

}