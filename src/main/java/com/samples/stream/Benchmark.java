package com.samples.stream;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Benchmark {
    public static void main(final String[] args) {
        final Benchmark benchmark = new Benchmark();
        final List<Person> people = Person.load();

        runWarmUp(20, people);

        benchmark.run("Count vowels   (loop)  ", p -> countVowelsLoop(p), people);
        benchmark.run("Count vowels  (stream) ", p -> countVowelsStream(p), people);
        benchmark.run("Count vowels (parallel)", p -> countVowelsParallel(p), people);
        System.out.println();
        benchmark.run("Count duplicates   (loop)  ", p -> countDistinctBirthDatesLoop(p), people);
        benchmark.run("Count duplicates  (stream) ", p -> countDistinctBirthDatesStream(p), people);
        benchmark.run("Count duplicates (parallel)", p -> countDistinctBirthDatesParallel(p), people);
        System.out.println();
        benchmark.run("Count duplicates   (loop)  ", p -> selectRandomPeopleLoop(p), people);
        benchmark.run("Count duplicates  (stream) ", p -> selectRandomPeopleStream(p), people);
        benchmark.run("Count duplicates (parallel)", p -> selectRandomPeopleParallel(p), people);
        System.out.println();
    }

    private static void runWarmUp(final int count, final List<Person> people) {
        for (int i = 0; i < count; i++) {
            countVowelsLoop(people);
            countVowelsStream(people);
            countVowelsParallel(people);
            countDistinctBirthDatesLoop(people);
            countDistinctBirthDatesStream(people);
            countDistinctBirthDatesParallel(people);
            selectRandomPeopleLoop(people);
            selectRandomPeopleStream(people);
            selectRandomPeopleParallel(people);
        }
    }

    private void run(final String text, final Consumer<List<Person>> consumer, final List<Person> people) {
        long start = System.nanoTime();
        consumer.accept(people);
        long elapsed = System.nanoTime() - start;
        System.out.println(text + " ran for " + NumberFormat.getInstance().format(elapsed) + " nanos");
    }

    private static int countVowelsLoop(final List<Person> people) {
        int count = 0;
        for (final Person p : people) {
            if (p.getEmail() != null || p.getPhone() != null) {
                count += vowels(p.getFirstName()) + vowels(p.getLastName());
            }
        }
        return count;
    }

    private static int countVowelsStream(final List<Person> people) {
        return people.stream()
                .mapToInt(p -> vowels(p.getFirstName()) + vowels(p.getLastName()))
                .sum();
    }

    private static int countVowelsParallel(final List<Person> people) {
        return people.parallelStream()
                .mapToInt(p -> vowels(p.getFirstName()) + vowels(p.getLastName()))
                .sum();
    }

    private static int countDistinctBirthDatesLoop(final List<Person> people) {
        final Set<LocalDate> unique = new HashSet<>();
        for (final Person p : people) {
            unique.add(p.getBirthdate());
        }
        return unique.size();
    }

    private static int countDistinctBirthDatesStream(final List<Person> people) {
        final Map<LocalDate, Long> unique =
                people.stream()
                        .collect(Collectors.groupingBy(Person::getBirthdate, Collectors.counting()));
        return unique.size();
    }

    private static int countDistinctBirthDatesParallel(final List<Person> people) {
        final Map<LocalDate, Long> unique =
                people.stream()
                        .collect(Collectors.groupingBy(Person::getBirthdate, Collectors.counting()));
        return unique.size();
    }

    private static Set<Person> selectRandomPeopleLoop(final List<Person> people) {
        final Random rand = new Random();
        final Set<Person> randomPeople = new HashSet<>();
        final int n = people.size();
        while (randomPeople.size() < 100) {
            final Person person = people.get(rand.nextInt(n));
            randomPeople.add(person);
        }
        return randomPeople;
    }

    private static Set<Person> selectRandomPeopleStream(final List<Person> people) {
        final Random rand = new Random();
        final Set<Person> randomPeople = rand.ints(500, 0, people.size())
                .distinct()
                .limit(100)
                .mapToObj(people::get)
                .collect(Collectors.toSet());
        return randomPeople;
    }

    private static Set<Person> selectRandomPeopleParallel(final List<Person> people) {
        final Random rand = new Random();
        final Set<Person> randomPeople = rand.ints(500, 0, people.size())
                .parallel()
                .distinct()
                .limit(100)
                .mapToObj(i -> people.get(i))
                .collect(Collectors.toSet());
        return randomPeople;
    }

    private static int vowels(final String s) {
        int vowels = 0;
        for (int i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case 'A':
                case 'a':
                case 'E':
                case 'e':
                case 'I':
                case 'i':
                case 'O':
                case 'o':
                case 'U':
                case 'u':
                    vowels++;
                    break;
            }
        }
        return vowels;
    }
}
