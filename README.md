# README #

blah blah bal. blah blah. blah blah.

### Prereq ###
* JDK 8

### Quick start ###

* Clone the repository

`git clone https://bitbucket.org/vincintz/java-streams-basics`

* Compile

`gradlew clean build`


### How do I view the solution ###

`git checkout solution`